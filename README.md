**Computing the Root Mean Square Error (RMSE)**


---

This class computes the Root Mean Square Error (RMSE) between numerically approximated derivatives
  using a parabolic fit and the closed form derivatives at these points. It then finds the RMSE for the
  other previously implemented numerical approximations of the derivative, such as the 5-point stencil,
  the symmetric difference quotient, and the two point slope (with the midpoint chosen as the domain). An
  analysis between the different numerical estimates of the derivative and their corresponding RMSE values is made
  in a separate pdf file.
  
  The parabolic derivatives are computed by finding values of "a" and "b" corresponding to the equation
  y = ax^2 + bx + c, and then finding the derivative of that fit, namely 2ax + b, at a specified point.
  The formulas for computing "a" and "b" in the parabolic fit are obtained from
  Mathematica.
  For this lab, the Gaussian function is used, and numerical approximations of the derivative at all
  points in the domain are computed by assigning the derivative value obtained by the parabolic fit
  at each three points to the midpoint. The beginning and endpoints are special cases where
  the derivative of the parabolic fit is assigned to the first point and third point, respectively.
  
  The code takes in values for the starting value of the range, the ending value, and the number of
  intervals in between the two. It outputs the RMSE values for each of the four numerical approximations
  of the derivative. It also prints out delta x, which is the space between two adjacent x-values.
  
  The Point class, comprising of an x and y pair (both doubles) is also used in this program to
  hold the values of x-y points.