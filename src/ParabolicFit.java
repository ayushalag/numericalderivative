import java.util.Scanner;

/**
 * This class computes the Root Mean Square Error (RMSE) between numerically approximated derivatives
 * using a parabolic fit and the closed form derivatives at these points. It then finds the RMSE for the
 * other previously implemented numerical approximations of the derivative, such as the 5-point stencil,
 * the symmetric difference quotient, and the two point slope (with the midpoint chosen as the domain). An
 * analysis between the different numerical estimates of the derivative and their corresponding RMSE values is made
 * in a separate pdf file.
 * 
 * The parabolic derivatives are computed by finding values of "a" and "b" corresponding to the equation
 * y = ax^2 + bx + c, and then finding the derivative of that fit, namely 2*ax + b, at a specified point.
 * The formulas for computing "a" and "b" in the parabolic fit are obtained from
 * Mathematica.
 * For this lab, the Gaussian function is used, and numerical approximations of the derivative at all
 * points in the domain are computed by assigning the derivative value obtained by the parabolic fit
 * at each three points to the midpoint. The beginning and endpoints are special cases where
 * the derivative of the parabolic fit is assigned to the first point and third point, respectively.
 * 
 * The code takes in values for the starting value of the range, the ending value, and the number of
 * intervals in between the two. It outputs the RMSE values for each of the four numerical approximations
 * of the derivative. It also prints out delta x, which is the space between two adjacent x-values.
 * 
 * The Point class, comprising of an x and y pair (both doubles) is also used in this program to
 * hold the values of x-y points.
 * 
 * @author AyushAlag
 * @creationDate February 3, 2018
 *
 */

public class ParabolicFit 
{
    
    /**
     * Finds the parabolic fit to three inputted points (p1, p2, p3) using formulas for "a" and "b"
     * obtained from mathematica. These a and b values correspond to the equation y = ax^2 + bx + c, 
     * and the derivative can then be computed from this fit as y' = 2ax + b. This derivative
     * is then applied at "derivLoc" and its value is returned.
     * 
     * @param p1 The first Point object passed in that is used to make the parabolic fit
     * @param p2 The second Point object passed in that is used to make the parabolic fit
     * @param p3 The third Point object passed in that is used to make the parabolic fit
     * @param derivLoc The Point at which the numerical approximation of the derivative (using
     * the curve fit method) is computed
     * 
     * @return the value of the numerically approximated derivative computed at "derivLoc", which
     * is of type double.
     */
    private double parabolicFitDeriv(Point p1, Point p2, Point p3, Point derivLoc)
    {
        //extract x and y vals
        double x1 = p1.getX();
        double y1 = p1.getY();
        double x2 = p2.getX();
        double y2 = p2.getY();
        double x3 = p3.getX();
        double y3 = p3.getY();
        
        //formulas for a and b taken from mathematica
        double a = - ( (x3*y1 + x1*y2 + x2*y3 - x2*y1 - x3*y2 - x1*y3)/( (x2-x1) * (x2-x3) * (x3-x1) ));
        
        double b = - ( (Math.pow(x2, 2) * y1 + Math.pow(x3 , 2) * y2 + Math.pow (x1 , 2) * y3
                - Math.pow(x3, 2) * y1 - Math.pow(x1, 2) * y2 - Math.pow(x2, 2) * y3)
                /((x1 - x2) * (x1-x3) * (x2-x3)) );
        
        //xVal is where the derivative is being calculated
        double xVal = derivLoc.getX();
        
        //closed form deriv of y = ax^2 + bx + c (the parabolic fit to the points)
        double derivAtX = 2 * a * xVal + b;
        
        return derivAtX;
    }
    
    /**
     * Uses the starting and ending points as well as the number of intervals in between the two
     * to generate a domain of Points on which derivatives (and their approximations) will be computed.
     * @param start the double starting value of the domain
     * @param end the double ending value of the domain
     * @param intervals the number of spaces between x-values in the domain (number of points - 1).
     * @return an array of Point objects containing the x-values in the domain and their corresponding
     * y-values (for the Gaussian function).
     */
    public Point[] getPointsInDomain(double start, double end, int intervals)
    {
        Point[] points = new Point[intervals+1];
        
        for (int i = 0; i<points.length; i++)
        {
            double delta = (end-start)/(intervals);
            
            double xVal = start + delta*i;
            
            //y-values for the Gaussian function
            double yVal = Math.exp(-xVal*xVal);
            
            points[i] = new Point(xVal,yVal);
        }
        return points;
    }
    
    /**
     * Cycles through the points in the domain, passing each set of three points into the private
     * helper method "parabolicFitDeriv", which returns and estimate for the derivative. 
     * This derivative approximation is then set to the midpoint of the three points used to compute it. 
     * The beginning and ends are special cases: the numerical derivative of first set of three 
     * is assigned to both the second and first points and the derivative of the last set of three
     * is assigned to both the second-to-last point and the last point.
     * 
     * @param domain the Points passed in to the program, on which the numerical approximation
     * to the derivatives are computed
     * @return the array of Points, with each y-value representing the approximation to the derivative
     * found using the parabolic fit. The x-value is the domain for which the the approximation is
     * chosen to be valid (eg midpoint for all cases except the first and last two points).
     */
    public Point[] getParabolicDerivatives(Point[] domain)
    {
        Point[] derivs = new Point[domain.length];
        
        //beginning point is a special case - assigning derivative to the first point
        
        double derivFirstPoint = parabolicFitDeriv(domain[0], domain[1], domain[2], domain[0]); //set to first point
        derivs[0] = new Point(domain[0].getX(), derivFirstPoint);
        
        //general case - assigning to the midpoint
        for (int i = 1; i<derivs.length-1; i++)
        {
            double deriv = parabolicFitDeriv(domain[i-1], domain[i], domain[i+1], domain[i]);//set to midpoint
            double xValAtDeriv = domain[i].getX();
            
            derivs[i] = new Point(xValAtDeriv, deriv);
        }
        
        //special case - assigning to the last point
        int len = domain.length;
        double derivLastPoint = parabolicFitDeriv(domain[len-3], domain[len-2], domain[len-1], domain[len-1]);
        
        derivs[derivs.length-1] = new Point(domain[len-1].getX(), derivLastPoint);
        
        return derivs;
    }
    
    /**
     * Computes the symmetric difference quotient at the given Points (in "domain")
     * Assigns the value of the derivative approximation to the middle point, thus omitting
     * the first and last x-values in the domain.
     * @param domain the Points in the domain the function which are used for the symmetric
     * difference quotient
     * @return the array of Points containing the values of the symmetric difference quotient for the y-values
     * along with their corresponding x-values
     */
    public Point[] symmetricDiffQuotient(Point[] domain)
    {
        Point[] diffQuotientDerivs = new Point[domain.length-2];
        
        for (int i = 1; i<domain.length-1; i++)
        {
            double xVal = domain[i].getX();
            double deriv = (domain[i+1].getY() - domain[i-1].getY())/(domain[i+1].getX()-domain[i-1].getX());
            
            diffQuotientDerivs[i-1] = new Point(xVal, deriv);
        }
        return diffQuotientDerivs;
    }
    
    /**
     * Computes the five-point stencil derivative approximation at the given Points (in "domain")
     * Assigns the value of the derivative approximation to the middle point, thus omitting the first two
     * and last two x-values in the domain.
     * @param domain the array of Points (representing the domain of the function) 
     * which are used to compute the stencil
     * @return the array of Points containing the values of the five-point stencil derivative
     * approximation for the y-values along with their corresponding x-values
     */
    public Point[] fivePointStencil(Point[] domain)
    {
        Point[] stencilVals = new Point[domain.length-4];
        
        for (int i = 2; i<domain.length-2; i++)
        {
            double deltaX = domain[i].getX() - domain[i-1].getX();
            
            double stencilVal = (8*domain[i+1].getY() - (8*domain[i-1].getY()) + 
                    domain[i-2].getY() - domain[i+2].getY())/(12*deltaX);
            
            double xVal = domain[i].getX();
            
            stencilVals[i-2] = new Point(xVal, stencilVal);
        }
        return stencilVals;
    }
    
    /**
     * Returns the Points containing the numerical derivatives computed using the two-point slope method
     * along with their corresponding x-values which, in this case, are the midpoints of the two x-values
     * used to find the slopes.
     * 
     * The last point is lost (since there is no further value that can be used to compute the slope).
     * 
     * @param domain the array of Points used to compute the slopes
     * @return the array of Points containing the values of the numerical derivatives
     */
    public Point[] getSlopeMidpoint(Point[] domain)
    {
        Point[] slopeMidpoint = new Point[domain.length-1];
        
        for (int i = 0; i<domain.length-1; i++)
        {
            double yChange = domain[i+1].getY() - domain[i].getY();
            double xChange = domain[i+1].getX() - domain[i].getX();
            
            double slope = yChange/xChange;
            
            double midpoint = (domain[i+1].getX() + domain[i].getX())/2;
            
            slopeMidpoint[i] = new Point(midpoint, slope);
        }
        return slopeMidpoint;
    }
    
    /**
     * Computes the closed-form derivatives at the points at which 
     * the numerical derivative approximations (which are passed in as "numericalVals") 
     * are calculated.
     * @param numericalVals the array of Points which dictate where the derivatives should
     * be computed (we want the domains of the numerical derivatives to correspond with those
     * of the closed form solution so we can compare the two methods with RMSE).
     * @return the array of Points holding the closed form derivative values.
     */
    public Point[] getClosedFormCounterpart(Point[] numericalVals)
    {
        Point[] derivs = new Point[numericalVals.length];
        
        for (int i = 0; i<numericalVals.length; i++)
        {
           //derivative taken at the x-values that are used in "numericalVals"
           double xVal = numericalVals[i].getX();
           
           double deriv = -2 * xVal * Math.exp(-xVal * xVal); //closed form derivative for Gaussian
           
           derivs[i] = new Point(xVal, deriv);
        }
        return derivs;
    }
    
    /**
     * Computes the root mean square error between the closed derivative values and a numerical
     * approximation of the derivative.
     * The RMSE is computed by summing the squared difference between each numerical and closed-form
     * value. The aggregate is averaged and then square rooted.
     * 
     * @precondition the number of elements in each array ("closed" and "numerical") should match; furthermore,
     * the x-values in each array should be aligned. The getClosedFromCounterpart method is what satisfies
     * this precondition.
     * @param closed the array of Points containing the values of the closed form derivative
     * @param numerical the array of Points containing the values of the numerically computed derivatives
     * @return the double root mean square error between the closed-form and numerical derivatives
     */
    public double getRootMeanSquareError(Point[] closed, Point[] numerical)
    {
        double totalError = 0;//sums individual errors
        
        for (int i = 0; i<closed.length; i++)
        {
            totalError += Math.pow( (numerical[i].getY() - closed[i].getY()) , 2);
        }
        
        double averageSquaredError = totalError/(closed.length);
        return Math.pow(averageSquaredError, 0.5); //returns square root of aggregate
    }
    
    
    /**
     * Driver method for this class.
     * 
     * It first prompts the user for the starting and ending x-values in the domain along with
     * the number of intervals between the two. The program then uses this information 
     * to create an array of Point objects that contain the x-y pairs for all values in the domain. 
     * This is done using the "getPointsInDomain" method. 
     * 
     * The program then computes each numerical
     * approximation to the derivative, the closed-form solutions at the points for which the numerical
     * estimate is valid, and the RMSE between the two. This is done for the parabolic fit, symmetric
     * difference quotient, stencil, and slope (with the midpoint as the domain).
     * 
     * The RMSE values along with delta x (the space between two adjacent x-values) are printed
     * to the console and used for a separate analysis.
     */
    public static void main (String [] args)
    {
        ParabolicFit f = new ParabolicFit();
        
        //From the reader, the code obtains the starting point and endpoint of the domain 
        //as well as how many intervals are between the two
        
        Scanner in = new Scanner(System.in);
        
        System.out.println("Enter a value for the starting value in the domain");
        double start = in.nextDouble();
        
        System.out.println("Enter a value for the ending value in the domain");
        double end = in.nextDouble();
        
        System.out.println("Enter a value for the number of intervals");
        int intervals = in.nextInt();
        
        //uses the starting point, endpoint, and number of intervals to generate the domain
        //of the function, which are the x-values being used to compute the derivatives
        Point[] domain = f.getPointsInDomain(start, end, intervals);
        
        
        //In the next blocks of code, the numerical derivatives and the corresponding closed
        //form solutions are computed, and the RMSE between the two is found and printed to the console
        
        Point[] parabolicDerivs = f.getParabolicDerivatives(domain);
        Point[] closedAtParabolic = f.getClosedFormCounterpart(parabolicDerivs);
        System.out.println("RMSE for Parabolic is " 
                + f.getRootMeanSquareError(closedAtParabolic, parabolicDerivs));
        
        
        Point[] symmetricDerivs = f.symmetricDiffQuotient(domain);
        Point[] closedAtSymmetric = f.getClosedFormCounterpart(symmetricDerivs);
        System.out.println("RMSE for Symmetric is " 
                + f.getRootMeanSquareError(closedAtSymmetric, symmetricDerivs));
                
        
        Point[] stencilDerivs = f.fivePointStencil(domain);
        Point[] closedAtStencil = f.getClosedFormCounterpart(stencilDerivs);
        System.out.println("RMSE for Stencil is " 
                + f.getRootMeanSquareError(closedAtStencil, stencilDerivs));
                
        
        Point[] slopeMidpointDerivs = f.getSlopeMidpoint(domain);
        Point[] closedAtMidpoint = f.getClosedFormCounterpart(slopeMidpointDerivs);
        System.out.println("RMSE for Two-Point slope at midpoint is " 
                + f.getRootMeanSquareError(closedAtMidpoint, slopeMidpointDerivs));
        
        //finds and prints delta x
        double deltaX = (end-start)/intervals;
        System.out.println("Delta x is " + deltaX);
    }

}