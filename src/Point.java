/**
 * Class that is used by ParabolicFit to hold x-y pairs (of double type)
 * @author AyushAlag
 * @creationDate February 3, 2018
 *
 */
public class Point 
{
    private double xVal;
    private double yVal;
    
    /**
     * Sets the inputted parameters to the instance variables holding the x and y values of the point
     * @param x the (double) x-value inputted
     * @param y the (double) y-value inputted
     */
    public Point (double x, double y)
    {
        xVal = x;
        yVal = y;
    }
    
    /**
     * Accessor method for the instance variable xVal
     * Returns the value of xVal
     * @return xVal (a double type)
     */
    public double getX()
    {
        return xVal;
    }
    
    /**
     * Accessor method for the instance variable yVal
     * Returns the value of yVal
     * @return yVal (a double type)
     */
    public double getY()
    {
        return yVal;
    }
}
